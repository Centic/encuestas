class Api::V1::ItacaController < ApplicationController
	def syncdb
		dbdata =
		{
			last: 123456789,
			operarios: [
				{idOperario: "oper1", login: "juanjo.centic", password: "123456", nombre: "Juanjo Franco"},
				{idOperario: "oper2", login: "pedro.centic", password: "123456", nombre: "Pedro Arques"},
				{idOperario: "oper3", login: "joaquin.centic", password: "123456", nombre: "Joaquin Lasheras"}
			],
			cultivos:[
				{id: "alav34", finca:"Los Alamos", parcela: "ALM3", notas: "parcela muy seca", variedad: "Pepitos Rubios", marco: "3x7"},
				{id: "alav35", finca:"Los Alamos", parcela: "ALM3", notas: "parcela muy seca", variedad: "Naftarina", marco: "7x7"},
				{id: "alav36", finca:"Los Alamos", parcela: "ALM3", notas: "parcela muy seca", variedad: "Pera Ercolini" , marco: "3x3"}
			],
			tareas:[
				{idTarea: "t01", descripcion: "Fumigar hojas"},
				{idTarea: "t02", descripcion: "Fumigar sulfatando"},
				{idTarea: "t03", descripcion: "Fumigar bases"}
			],
			maquinas: [
				{idMaquina: "m01", nombre: "Lambo", isApero: false},
				{idMaquina: "m02", nombre: "Dosificador ERC", isApero: true, qty: 2000},
				{idMaquina: "m03", nombre: "Lambo", isApero: false},
				{idMaquina: "maq5", nombre: "Chevy", isApero: false},
				{idMaquina: "maqape1", nombre: "Aveo", isApero: true, qty: 1500},
			],
			productos: [
				{idProducto: "salfu045", nombre: "Salfumatado de Berilio", stock: 10, materias: "materia1,materia2,materia3"},
				{idProducto: "salfu046", nombre: "Salfumatado de Berilio", stock: 0, materias: "materia1,materia2,materia3"},
				{idProducto: "salfu047", nombre: "Salfumatado de Berilio", stock: 5, materias: "materia1,materia2,materia3"},
				{idProducto: "prod1", nombre: "SalProducto 1", stock: 5, materias: "materia1,materia2,materia3"},
				{idProducto: "prod2", nombre: "Producto 234 de Berilio", stock: 5, materias: "materia1,materia2,materia3"},
				{idProducto: "prod3", nombre: "Producto123 de Berilio", stock: 5, materias: "materia1,materia2,materia3"}
			],
			enfermedades: [
				{idEnfermedad: "enf1", nombre: "Caida de ojos picudos"},
				{idEnfermedad: "enf2", nombre: "Caida de ojos tuertos"},
				{idEnfermedad: "enf3", nombre: "Caida de ojos picudos 2"}
			],
			dosis: [
				{idProducto: "salfu045", idEnfermedad: "enf1", dosis: 1.8, mulDosis: 1000},
				{idProducto: "salfu045", idEnfermedad: "enf2", dosis: 1.8, mulDosis: 1000},
				{idProducto: "prod1", idEnfermedad: "enf2", dosis: 1.2, mulDosis: 1000},
				{idProducto: "prod2", idEnfermedad: "enf2", dosis: 1.4, mulDosis: 1000},
				{idProducto: "prod3", idEnfermedad: "enf2", dosis: 1.6, mulDosis: 1000},
				{idProducto: "salfu046", idEnfermedad: "enf1", dosis: 1.8, mulDosis: 1000}
			],
			densidades: [
				{idDensidad: "1500", idMaquina: "m01", idApero: "m02", velocidad: 6.5, presion: 4.8, marcha: 3, boquilla: "iso", rpm: 5600},
				{idDensidad: "1500", idMaquina: "m01", idApero: "m02", velocidad: 4.5, presion: 1.8, marcha: 2, boquilla: "porcelana", rpm: 6600},
				{idDensidad: "1400", idMaquina: "m01", idApero: "m02", velocidad: 5.5, presion: 6.8, marcha: 3, boquilla: "iso", rpm: 6600},
				{idDensidad: "1400", idMaquina: "m01", idApero: "m02", velocidad: 4.5, presion: 3.8, marcha: 2, boquilla: "porcelana", rpm: 7600},
				{idDensidad: "1400", idMaquina: "maq5", idApero: "maqape1", velocidad: 4.5, presion: 3.8, marcha: 2, boquilla: "porcelana", rpm: 7600},
				{idDensidad: "1400", idMaquina: "maq5", idApero: "maqape1", velocidad: 5.5, presion: 1.8, marcha: 3, boquilla: "iso", rpm: 5600},
				{idDensidad: "1500", idMaquina: "maq5", idApero: "maqape1", velocidad: 6.5, presion: 2.8, marcha: 2, boquilla: "porcelana", rpm: 7600},
				{idDensidad: "1500", idMaquina: "maq5", idApero: "maqape1", velocidad: 7.5, presion: 4.8, marcha: 3, boquilla: "iso", rpm: 5600},
				{idDensidad: "1200", idMaquina: "maq5", idApero: "maqape1", velocidad: 8.5, presion: 5.8, marcha: 2, boquilla: "porcelana", rpm: 7600},
				{idDensidad: "1200", idMaquina: "maq5", idApero: "maqape1", velocidad: 9.5, presion: 7.8, marcha: 3, boquilla: "iso", rpm: 5600},
				{idDensidad: "1200", idMaquina: "m01", idApero: "m02", velocidad: 7.5, presion: 7.8, marcha: 3, boquilla: "iso", rpm: 8600}
			]
		}
		render :json => dbdata
	end

	def recomen
		dbdata = 
		[	{
				idRecomendacion: "cod1",
				idOperario: "oper1",
				idCultivoParcela: "alav34", 
				fecha:"2014-12-15T09:30:00.000Z", 
				idTarea: "t01", 
				idMaquina: "maq5", 
				idApero: "maqape1",
				densidad: "1400",
				boquilla: "iso",
				mezclaRecomendacion:[
					{idProducto: "prod1", idEnfermedad: "enf2", dosis: 1.5, mulDosis: 1000}, 
					{idProducto: "prod2", idEnfermedad: "enf2", dosis: 5, mulDosis: 100},  
					{idProducto: "prod3", idEnfermedad: "enf2", dosis: 1, mulDosis: 1000} 
				]
			},
			{
				idRecomendacion: "cod2",
				idOperario: "oper1",
				idCultivoParcela: "alav34", 
				fecha:"2014-12-16T09:30:00.000Z", 
				idTarea: "t03", 
				idMaquina: "maq5", 
				idApero: "maqape1",
				densidad: "1400",
				boquilla: "iso",
				mezclaRecomendacion:[
					{idProducto: "prod1", idEnfermedad: "enf2", dosis: 1.5, mulDosis: 1000}, 
					{idProducto: "prod2", idEnfermedad: "enf2", dosis: 5, mulDosis: 100},  
					{idProducto: "prod3", idEnfermedad: "enf2", dosis: 1, mulDosis: 1000} 
				]
			},
			{
				idRecomendacion: "cod3",
				idOperario: "oper1",
				idCultivoParcela: "alav34", 
				fecha:"2014-12-18T09:30:00.000Z", 
				idTarea: "t02", 
				idMaquina: "maq5", 
				idApero: "maqape1",
				densidad: "1400",
				boquilla: "iso",
				mezclaRecomendacion:[
					{idProducto: "prod1", idEnfermedad: "enf2", dosis: 1.5, mulDosis: 1000}, 
					{idProducto: "prod2", idEnfermedad: "enf2", dosis: 5, mulDosis: 100},  
					{idProducto: "prod3", idEnfermedad: "enf2", dosis: 1, mulDosis: 1000} 
				]
			},
			{
				idRecomendacion: "cod4",
				idOperario: "oper1",
				idCultivoParcela: "alav34", 
				fecha:"2014-12-20T09:30:00.000Z", 
				idTarea: "t03", 
				idMaquina: "maq5", 
				idApero: "maqape1",
				densidad: "1400",
				boquilla: "iso",
				mezclaRecomendacion:[
					{idProducto: "prod1", idEnfermedad: "enf2", dosis: 1.5, mulDosis: 1000}, 
					{idProducto: "prod2", idEnfermedad: "enf2", dosis: 5, mulDosis: 100},  
					{idProducto: "prod3", idEnfermedad: "enf2", dosis: 1, mulDosis: 1000} 
				]
			}
		]
		render :json => dbdata
	end

	def tratamiento
		render :json => {
    		error: 200,
    		message: "Tratamiento almacenado"
		}
	end
end
