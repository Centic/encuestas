class PollsController < ApplicationController

	skip_before_filter :verify_authenticity_token, only: [:preparepoll]
	def preparepoll

		poll = Poll.find(params[:poll_id])
		if !poll
			render plain: '{"Error":1003, "Message":"No pollpoll,  requested"}, sort_keys=False)' and return
		end
		invitation_param = params[:invitation];
		if !invitation_param
			render plain: '{"Error":1001, "Message":"No invitation requested"}, sort_keys=False)' and return
		end
		token_param = params[:validation]
		if !token_param
			render plain: '{"Error":1002, "Message":"No validation requested"}, sort_keys=False)' and return
		end
		@invitation = Invitation.new
		@invitation.poll_id = poll.id
		@invitation.invitation = invitation_param
		@invitation.validation_token = token_param

		if @invitation.save
			render plain: '{"Error":0, "Message":"Encuesta preparada para "}, sort_keys=False)' and return
		end
		render plain: '{"Error":1003, "Message":"No data saved"}, sort_keys=False)' and return
	end

	def index
		@polls = Poll.all
	end

	def show
		@poll = Poll.find(params[:id])
	end

	def new
		@poll = Poll.new
	end

	def create
		@poll = Poll.new(poll_params)

		if @poll.save
			redirect_to @poll
		else
			render 'new'
		end
	end

	def edit
		@poll = Poll.find(params[:id])
	end

	def update
		@poll = Poll.find(params[:id])
		if poll.update(poll_params)
			redirect_to @poll
		else
			render 'edit'
		end
	end

	def invitations
		@invitations = Invitation.all
		render json: @invitations
	end

	def destroy
		@poll = Poll.find(params[:id])
		@poll.destroy
		redirect_to polls_path
	end

	def fill
		@poll = Poll.find(params[:poll_id])
		@invitation = Invitation.where(invitation:params[:invitation]).first
		validation = params[:validation]
		if !validation
			render plain: '{"Error":1003, "Message":"Need validation parmas"}, sort_keys=False)' and return
		end
		if !@invitation
			render plain: '{"Error":1003, "Message":"No invitation found"}, sort_keys=False)' and return
		end

		@invitation.validation_mobile = validation
		@invitation.save

	end

	def check_fill
		@invitation = Invitation.where(invitation: params[:invitation]).first
		if !@invitation
			render plain: '{"Error":1003, "Message":"No invitation found"}, sort_keys=False)' and return
		end

		url = 'http://testing.interface.centic.es/checker/validate/externalaction/'+@invitation.invitation+'/?token='+@invitation.validation_token+'&validation='+@invitation.validation_mobile

		response = HTTParty.get(url)
	end

	private
  	def poll_params
    	params.require(:poll).permit(:title, :text)
  	end
end
