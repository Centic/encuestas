class QuestionsController < ApplicationController
	def index
		render plain: params.to_s
	end

  	def create
    	@poll = Poll.find(params[:poll_id])
    	@question = @poll.questions.create(question_params)
    	redirect_to poll_path(@poll)
  	end
 
  	private
    def question_params
      params.require(:question).permit(:question)
    end
end
