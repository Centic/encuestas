class ResponsesController < ApplicationController
	def index
		render plain: params.to_s
	end

  	def create
    	@poll = Poll.find(params[:poll_id])
    	@question = Question.find(params[:question_id])
    	@response = @question.responses.create(response_params)
    	redirect_to poll_path(@poll)
  	end
 
  	private
    def response_params
      params.require(:response).permit(:text, :value)
    end
end
