class Poll < ActiveRecord::Base
	has_many :questions
	has_many :invitations
	validates :title, presence: true,
                    length: { minimum: 5 }
end
