class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.string :invitation
      t.string :validation_token
      t.string :validation_mobile

      t.timestamps
    end
    add_index :invitations, :invitation, unique: true
  end
end
