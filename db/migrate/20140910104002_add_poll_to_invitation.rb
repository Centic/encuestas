class AddPollToInvitation < ActiveRecord::Migration
  def change
    add_reference :invitations, :poll, index: true
  end
end
